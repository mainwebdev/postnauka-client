import {Component, Input, OnInit} from '@angular/core';
import {DomSanitizer} from "@angular/platform-browser";

@Component({
  selector: 'preview-event',
  templateUrl: './preview-event.component.html',
  styleUrls: ['./preview-event.component.styl']
})
export class PreviewEventComponent implements OnInit {

  @Input() preview: any;

  constructor(private sanitizer: DomSanitizer) {
  }

  ngOnInit() {
  }

  sanitizeImage(image_path) {
    return this.sanitizer.bypassSecurityTrustStyle(`url(${image_path})`);
  }
}
