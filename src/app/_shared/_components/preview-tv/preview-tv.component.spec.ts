import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreviewTvComponent } from './preview-tv.component';

describe('PreviewTvComponent', () => {
  let component: PreviewTvComponent;
  let fixture: ComponentFixture<PreviewTvComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreviewTvComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreviewTvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
