import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {MainComponent} from "./_pages/main";
import {LoginComponent} from "./_pages/login";
import {NotFoundComponent} from "./_pages/not-found";
import {AuthGuard} from "./_services/auth/auth.guard";
import {AuthService} from "./_services/auth/auth.service";

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'search',
    loadChildren: 'app/search/search.module#SearchModule',
  },
  {
    path: 'events',
    loadChildren: 'app/event/event.module#EventModule',
    canActivate: [AuthGuard],
  },
  {
    path: 'tv',
    loadChildren: 'app/tv/tv.module#TvModule',
    canActivate: [AuthGuard],
  },
  {
    path: 'video',
    loadChildren: 'app/video/video.module#VideoModule',
    canActivate: [AuthGuard],
  },
  {
    path: 'books',
    loadChildren: 'app/book/book.module#BookModule',
    canActivate: [AuthGuard],
  },
  {
    path: 'animate',
    loadChildren: 'app/animate/animate.module#AnimateModule',
    canActivate: [AuthGuard],
  },
  {
    path: 'tests',
    loadChildren: 'app/game/game.module#GameModule',
    canActivate: [AuthGuard],
  },
  {
    path: 'talks',
    loadChildren: 'app/talk/talk.module#TalkModule',
    canActivate: [AuthGuard],
  },

  {
    path: 'themes',
    loadChildren: 'app/theme/theme.module#ThemeModule',
    canActivate: [AuthGuard],
  },
  {
    path: 'authors',
    loadChildren: 'app/author/author.module#AuthorModule',
    canActivate: [AuthGuard],
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [
    AuthGuard,
    AuthService
  ]
})
export class AppRoutingModule {
}
