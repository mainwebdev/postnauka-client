import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedModule} from "../_shared/shared.module";

import {SearchRoutingModule} from './search-routing.module';
import {ResultsPageComponent} from './_pages/results-page';

import {SearchService} from "../_shared/_services/search/search.service";

@NgModule({
  imports: [
    CommonModule,
    SearchRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [
    SearchService
  ],
  declarations: [
    ResultsPageComponent
  ]
})
export class SearchModule {
}
