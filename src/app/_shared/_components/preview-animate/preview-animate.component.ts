import {Component, Input, OnInit} from '@angular/core';
import {DomSanitizer} from "@angular/platform-browser";
import {GridService} from "../../_services/grid/grid.service";

@Component({
  selector: 'preview-animate',
  templateUrl: './preview-animate.component.html',
  styleUrls: ['./preview-animate.component.styl']
})
export class PreviewAnimateComponent implements OnInit {

  @Input() preview:any;
  extractLink = GridService.extractLink;

  constructor(private sanitizer:DomSanitizer) {
  }

  ngOnInit() {
  }

  sanitizeImage(image_path) {
    return this.sanitizer.bypassSecurityTrustStyle(`url(${image_path})`);
  }

}
