import {Injectable} from '@angular/core';

import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {User} from "../../_interfaces/user";
import 'rxjs/add/operator/map';

@Injectable()
export class AuthService {
  readonly USER_LOCAL_STORAGE_KEY = 'currentUser';

  token: string;

  constructor(private http: HttpClient) {
    let currentUser = JSON.parse(localStorage.getItem(this.USER_LOCAL_STORAGE_KEY));
    this.token = currentUser && currentUser.token;
  }

  login(email: string, password: string): Observable<boolean> {
    return this.http.post<any>('/v1/auth/login', {email: email, password: password})
      .map((response) => {
        let token = response && response.token;
        if (token) {
          this.token = token;
          localStorage.setItem(this.USER_LOCAL_STORAGE_KEY, JSON.stringify(response));
          return true;
        } else {
          return false;
        }
      });
  }

  signin(email: string, password: string): Observable<User> {
    return this.http.post<User>('/v1/auth', {email: email, password: password});
  }

  logout(): void {
    this.token = null;
    localStorage.removeItem(this.USER_LOCAL_STORAGE_KEY);
  }

  getHttpOptions() {
    return {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + this.token
      })
    };
  }
}
