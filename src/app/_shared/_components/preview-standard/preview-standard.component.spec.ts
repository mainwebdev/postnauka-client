import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreviewStandardComponent } from './preview-standard.component';

describe('PreviewStandardComponent', () => {
  let component: PreviewStandardComponent;
  let fixture: ComponentFixture<PreviewStandardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreviewStandardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreviewStandardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
