import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap} from "@angular/router";
import 'rxjs/add/operator/switchMap';
import {GridService} from "../../../_shared/_services/grid/grid.service";

@Component({
  selector: 'app-view-page',
  templateUrl: './view-page.component.html',
  styleUrls: ['./view-page.component.styl']
})
export class ViewPageComponent implements OnInit {
  alias: string = '';
  currentPage = 1;
  pageCount = 1;
  items = [];

  constructor(private route: ActivatedRoute,
              private grid: GridService) {
  }

  ngOnInit() {
    this.route.paramMap
      .switchMap((params: ParamMap) => {
        this.alias = params.get('alias');
        return this.load(this.alias);
      })
      .subscribe(resp => this.updateItems(resp));
  }

  load(alias) {
    return this.grid.getItems('/v1/theme/' + alias, this.currentPage);
  }

  updateItems(resp) {
    this.pageCount = +resp.headers.get('x-pagination-page-count');
    this.currentPage++;
    this.items = this.items.concat(resp.body);
  }

  showMore() {
    if (this.currentPage <= this.pageCount) {
      this
        .load(this.alias)
        .subscribe(resp => this.updateItems(resp));
    }
  }
}
