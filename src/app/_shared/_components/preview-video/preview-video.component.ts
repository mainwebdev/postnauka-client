import {Component, OnInit, Input} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';
import {GridService} from "../../_services/grid/grid.service";

@Component({
  selector: 'preview-video',
  templateUrl: './preview-video.component.html',
  styleUrls: ['./preview-video.component.styl']
})
export class PreviewVideoComponent implements OnInit {

  @Input() preview:any;
  extractLink = GridService.extractLink;
  constructor(private sanitizer:DomSanitizer) {
  }

  ngOnInit() {
  }

  sanitizeImage(image_path) {
    return this.sanitizer.bypassSecurityTrustStyle(`url(${image_path})`);
  }
}
