import {Component, OnInit, Input} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'preview-standard',
  templateUrl: './preview-standard.component.html',
  styleUrls: ['./preview-standard.component.styl']
})
export class PreviewStandardComponent implements OnInit {

  @Input() preview:any;

  constructor(private sanitizer:DomSanitizer) {
  }

  ngOnInit() {
  }

  sanitizeImage(image_path) {
    return this.sanitizer.bypassSecurityTrustStyle(`url(${image_path})`);
  }
}
