import {Component, OnInit} from '@angular/core';
import {GridService} from "../../_shared/_services/grid/grid.service";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.styl']
})
export class MainComponent implements OnInit {
  currentPage = 1;
  pageCount = 1;
  items = [];

  constructor(private grid: GridService) {
  }

  ngOnInit() {
    this.load().subscribe(resp => this.updateItems(resp));
  }

  load() {
    return this.grid.getItems('/v1/feed', this.currentPage);
  }

  updateItems(resp) {
    this.pageCount = +resp.headers.get('x-pagination-page-count');
    this.currentPage++;
    this.items = this.items.concat(resp.body);
  }

  showMore() {
    if (this.currentPage <= this.pageCount) {
      this.load().subscribe(resp => this.updateItems(resp));
    }
  }
}
