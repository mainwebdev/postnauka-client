import {Component, Input, OnInit} from '@angular/core';
import {DomSanitizer} from "@angular/platform-browser";
import {GridService} from "../../_services/grid/grid.service";

@Component({
  selector: 'preview-game',
  templateUrl: './preview-game.component.html',
  styleUrls: ['./preview-game.component.styl']
})
export class PreviewGameComponent implements OnInit {

  @Input() preview: any;
  extractLink = GridService.extractLink;

  constructor(private sanitizer: DomSanitizer) {
  }

  ngOnInit() {
  }

  sanitizeImage(image_path) {
    return this.sanitizer.bypassSecurityTrustStyle(`url(${image_path})`);
  }
}
