import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AuthorRoutingModule} from './author-routing.module';
import {SharedModule} from "../_shared/shared.module";

import {IndexPageComponent} from './_pages/index-page';
import {ViewPageComponent} from './_pages/view-page';

@NgModule({
  imports: [
    CommonModule,
    AuthorRoutingModule,
    SharedModule
  ],
  declarations: [
    IndexPageComponent,
    ViewPageComponent,
  ]
})
export class AuthorModule {
}
