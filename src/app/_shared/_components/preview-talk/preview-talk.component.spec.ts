import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreviewTalkComponent } from './preview-talk.component';

describe('PreviewTalkComponent', () => {
  let component: PreviewTalkComponent;
  let fixture: ComponentFixture<PreviewTalkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreviewTalkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreviewTalkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
