import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ResultsPageComponent} from "./_pages/results-page";

const routes: Routes = [
  {
    path: '', component: ResultsPageComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SearchRoutingModule {
}
