import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from "../_shared/shared.module";
import {ThemeRoutingModule} from './theme-routing.module';

import {IndexPageComponent} from './_pages/index-page';
import {ViewPageComponent} from './_pages/view-page';

@NgModule({
  imports: [
    CommonModule,
    ThemeRoutingModule,
    SharedModule
  ],
  declarations: [
    IndexPageComponent,
    ViewPageComponent,
  ],
})
export class ThemeModule {
}
