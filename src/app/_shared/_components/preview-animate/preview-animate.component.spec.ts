import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreviewAnimateComponent } from './preview-animate.component';

describe('PreviewAnimateComponent', () => {
  let component: PreviewAnimateComponent;
  let fixture: ComponentFixture<PreviewAnimateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreviewAnimateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreviewAnimateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
