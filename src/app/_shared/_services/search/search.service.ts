import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpResponse} from "@angular/common/http";
import {Observable} from "rxjs/Observable";

@Injectable()
export class SearchService {

  constructor(private http: HttpClient) {
  }

  getItems(q, page = 1): Observable<HttpResponse<any>> {
    return this.http.get<Array<any>>(
      '/v1/search?q=' + q + '&page=' + page, {
        observe: 'response',
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
        })
      }
    );
  }
}
