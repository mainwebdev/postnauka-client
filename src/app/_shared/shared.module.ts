import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HttpClientModule} from "@angular/common/http";
import {RouterModule} from '@angular/router';

import {PreviewStandardComponent} from "./_components/preview-standard";
import {PreviewVideoComponent} from "./_components/preview-video";
import {PreviewAuthorComponent} from './_components/preview-author';
import {PreviewThemeComponent} from './_components/preview-theme';
import {PreviewEventComponent} from './_components/preview-event';
import {PreviewBookComponent} from './_components/preview-book';
import {PreviewAnimateComponent} from './_components/preview-animate';
import {PreviewTvComponent} from './_components/preview-tv';
import { PreviewGameComponent } from './_components/preview-game';
import { PreviewTalkComponent } from './_components/preview-talk';

import {GridComponent} from './_components/grid';

import {HeaderComponent} from './_components/header';

import {GridService} from "./_services/grid/grid.service";
import {SearchService} from "./_services/search/search.service";
import { FooterComponent } from './_components/footer/footer.component';


@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    HttpClientModule
  ],
  declarations: [
    GridComponent,
    HeaderComponent,
    PreviewStandardComponent,
    PreviewVideoComponent,
    PreviewAuthorComponent,
    PreviewThemeComponent,
    PreviewEventComponent,
    PreviewBookComponent,
    PreviewAnimateComponent,
    PreviewTvComponent,
    PreviewGameComponent,
    PreviewTalkComponent,
    FooterComponent,
  ],
  providers: [
    GridService,
    SearchService,
  ],
  exports: [
    GridComponent,
    HeaderComponent,
  ]
})
export class SharedModule {
}
