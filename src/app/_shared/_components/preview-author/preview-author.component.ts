import {Component, Input, OnInit} from '@angular/core';
import {DomSanitizer} from "@angular/platform-browser";

@Component({
  selector: 'preview-author',
  templateUrl: './preview-author.component.html',
  styleUrls: ['./preview-author.component.styl']
})
export class PreviewAuthorComponent implements OnInit {

  @Input() preview:any;

  constructor(private sanitizer:DomSanitizer) {
  }

  ngOnInit() {
  }

  sanitizeImage(image_path) {
    return this.sanitizer.bypassSecurityTrustStyle(`url(${image_path})`);
  }
}
