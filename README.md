# Frontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.3.

## Локальный сервер разработки

Если у вас есть доступ к серверу с локальным бекэндом, то запускайте ```npm start```.

Если доступа к локальному бекэнду у вас нет, запускайте ```npm start-web```.

После запуска локальный сервер с фронтом будет доступен здесь  [http://localhost:4200/](http://localhost:4200/)

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
