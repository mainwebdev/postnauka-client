import {Component, Input} from '@angular/core';
import {DomSanitizer} from "@angular/platform-browser";
import {GridService} from "../../_services/grid/grid.service";

@Component({
  selector: 'grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.styl']
})
export class GridComponent {
  @Input() items: Array<any>;

  isStandard = GridService.isStandard;
  isVideo = GridService.isVideo;
  isEvent = GridService.isEvent;
  isBook = GridService.isBook;
  isAnimate = GridService.isAnimate;
  isTv = GridService.isTv;
  isGame = GridService.isGame;
  isTalk = GridService.isTalk;
  isAuthor = GridService.isAuthor;
  isTheme = GridService.isTheme;

  extractAuthorLink = GridService.extractAuthorLink;
  extractLink = GridService.extractLink;

  constructor(private sanitizer: DomSanitizer) {
  }

  sanitizeCssImage(image_path) {
    return this.sanitizer.bypassSecurityTrustStyle(`url(${image_path})`);
  }
}
