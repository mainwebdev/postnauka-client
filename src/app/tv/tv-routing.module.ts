import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {IndexPageComponent} from "./_pages/index-page";
import {ViewPageComponent} from "./_pages/view-page";

const routes: Routes = [
  {
    path: '',
    component: IndexPageComponent
  },
  {
    path: ':id',
    component: ViewPageComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TvRoutingModule {
}
