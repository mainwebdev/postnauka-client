import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {SearchService} from "../../../_shared/_services/search/search.service";

@Component({
  selector: 'app-results-page',
  templateUrl: './results-page.component.html',
  styleUrls: ['./results-page.component.styl']
})
export class ResultsPageComponent implements OnInit {
  searchForm: FormGroup;
  currentPage = 1;
  pageCount = 1;
  items = [];

  constructor(private fb: FormBuilder,
              private search: SearchService) {
  }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.searchForm = this.fb.group({
      q: ['', Validators.required],
    });
    this.onChanges();
  }

  load(q) {
    return this.search.getItems(q, this.currentPage);
  }

  updateItems(resp) {
    this.pageCount = +resp.headers.get('x-pagination-page-count');
    this.currentPage = 1;
    this.items = resp.body;
  }

  onSubmit() {
    this.load(this.searchForm.get('q').value).subscribe(resp => this.updateItems(resp));
  }

  onChanges(): void {
    this.searchForm.get('q').valueChanges.subscribe(val => {
      this.load(val).subscribe(resp => this.updateItems(resp));
    });
  }
}
