import {Component, Input, OnInit} from '@angular/core';
import {DomSanitizer} from "@angular/platform-browser";

@Component({
  selector: 'preview-theme',
  templateUrl: './preview-theme.component.html',
  styleUrls: ['./preview-theme.component.styl']
})
export class PreviewThemeComponent implements OnInit {
  @Input() preview:any;

  constructor(private sanitizer:DomSanitizer) {
  }

  ngOnInit() {
  }

  sanitizeImage(image_path) {
    return this.sanitizer.bypassSecurityTrustStyle(`url(${image_path})`);
  }
}
