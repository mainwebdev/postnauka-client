import {Component, OnInit} from '@angular/core';
import {GridService} from "../../../_shared/_services/grid/grid.service";

@Component({
  selector: 'app-index-page',
  templateUrl: './index-page.component.html',
  styleUrls: ['./index-page.component.styl']
})
export class IndexPageComponent implements OnInit {
  readonly alias = 'tests';
  currentPage = 1;
  pageCount = 1;
  items = [];

  constructor(private grid: GridService) {
  }

  ngOnInit() {
    this.load().subscribe(resp => this.updateItems(resp));
  }

  load() {
    return this.grid.getItems('/v1/theme/' + this.alias, this.currentPage);
  }

  updateItems(resp) {
    this.pageCount = +resp.headers.get('x-pagination-page-count');
    this.currentPage++;
    this.items = this.items.concat(resp.body);
  }

  showMore() {
    if (this.currentPage <= this.pageCount) {
      this.load().subscribe(resp => this.updateItems(resp));
    }
  }

}
