import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpResponse} from "@angular/common/http";
import {Observable} from "rxjs/Observable";

@Injectable()
export class GridService {

  constructor(private http: HttpClient) {
  }

  getItems(apiUrl, page = 1): Observable<HttpResponse<any>> {
    return this.http.get<Array<any>>(
      apiUrl + '?page=' + page, {
        observe: 'response',
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
        })
      }
    );
  }

  static isStandard(item: any) {
    return !this.isVideo(item)
      && !this.isEvent(item)
      && !this.isBook(item)
      && !this.isTv(item)
      && !this.isAnimate(item)
      && !this.isGame(item)
      && !this.isTalk(item)
      && !this.isAuthor(item)
      && !this.isTheme(item);
  }

  static isVideo(item: any) {
    return item.category &&
      item.category[0] &&
      item.category[0].alias &&
      item.category[0].alias == 'video';
  }

  static isEvent(item: any) {
    return item.category &&
      item.category[0] &&
      item.category[0].alias &&
      item.category[0].alias == 'events';
  }

  static isBook(item: any) {
    return item.category &&
      item.category[0] &&
      item.category[0].alias &&
      item.category[0].alias == 'books';
  }

  static isAnimate(item: any) {
    return item.category &&
      item.category[0] &&
      item.category[0].alias &&
      item.category[0].alias == 'animate';
  }

  static isTv(item: any) {
    return item.category &&
      item.category[0] &&
      item.category[0].alias &&
      item.category[0].alias == 'tv';
  }

  static isGame(item: any) {
    return item.category &&
      item.category[0] &&
      item.category[0].alias &&
      item.category[0].alias == 'tests';
  }

  static isTalk(item: any) {
    return item.category &&
      item.category[0] &&
      item.category[0].alias &&
      item.category[0].alias == 'talks';
  }

  static isAuthor(item: any) {
    return item.type && item.type == 'author';
  }

  static isTheme(item: any) {
    return item.type && (item.type == 'themes' || item.type == 'category');
  }

  static extractLink(link) {
    return link.replace(/https:\/\/postnauka.ru\//i, '/')
  }

  static extractAuthorLink(link) {
    return link.replace(/\/author\//i, '/authors/')
  }

  static randomColor() {
    return '#' + (Math.random() * 0xFFFFFF << 0).toString(16);
  }
}
