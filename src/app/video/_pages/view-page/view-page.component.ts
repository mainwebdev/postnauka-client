import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap} from "@angular/router";
import 'rxjs/add/operator/switchMap';
import {PostService} from "../../../_shared/_services/post/post.service";

@Component({
  selector: 'app-view-page',
  templateUrl: './view-page.component.html',
  styleUrls: ['./view-page.component.styl']
})
export class ViewPageComponent implements OnInit {

  post: any;

  constructor(private route: ActivatedRoute,
              private postService: PostService) {
  }

  ngOnInit() {
    const id = (!this.route.snapshot.paramMap.get('id')) ? '' : this.route.snapshot.paramMap.get('id').trim();

    this.route.paramMap
      .switchMap((params: ParamMap) => {
        return this.postService.getItem(params.get('id'));
      })
      .subscribe(resp => {
        this.post = resp.body;
      });
  }

}
