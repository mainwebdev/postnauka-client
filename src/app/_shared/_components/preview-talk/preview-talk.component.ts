import {Component, Input, OnInit} from '@angular/core';
import {DomSanitizer} from "@angular/platform-browser";
import {GridService} from "../../_services/grid/grid.service";

@Component({
  selector: 'preview-talk',
  templateUrl: './preview-talk.component.html',
  styleUrls: ['./preview-talk.component.styl']
})
export class PreviewTalkComponent implements OnInit {

  @Input() preview: any;
  extractLink = GridService.extractLink;

  constructor(private sanitizer: DomSanitizer) {
  }

  ngOnInit() {
  }

  sanitizeImage(image_path) {
    return this.sanitizer.bypassSecurityTrustStyle(`url(${image_path})`);
  }
}
