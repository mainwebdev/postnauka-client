import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpResponse} from "@angular/common/http";
import {Observable} from "rxjs/Observable";

@Injectable()
export class PostService {

  constructor(private http: HttpClient) {
  }

  getItem(id): Observable<HttpResponse<any>> {
    return this.http.get<Array<any>>(
      '/v1/posts/' + +id, {
        observe: 'response',
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
        })
      }
    );
  }

}
