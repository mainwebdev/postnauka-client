import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {BookRoutingModule} from './book-routing.module';
import {SharedModule} from "../_shared/shared.module";
import {IndexPageComponent} from './_pages/index-page';
import {ViewPageComponent} from './_pages/view-page';
import {PostService} from "../_shared/_services/post/post.service";

@NgModule({
  imports: [
    CommonModule,
    BookRoutingModule,
    SharedModule

  ],
  declarations: [
    IndexPageComponent,
    ViewPageComponent
  ],
  providers: [
    PostService
  ]
})
export class BookModule {
}
