import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreviewAuthorComponent } from './preview-author.component';

describe('PreviewAuthorComponent', () => {
  let component: PreviewAuthorComponent;
  let fixture: ComponentFixture<PreviewAuthorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreviewAuthorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreviewAuthorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
